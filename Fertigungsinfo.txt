Auftraggeber:

	microdrones GmbH
	Nerzweg 5
	57072 Siegen
	0271-770038-0

technischer Ansprechpartner:

	Hartmut Dach
	h.dach@microdrones.com
	0271-770038-130


Baugruppe:          mdAntennaAdapter

Nutzen:             MFN20x_mdAntennaAdapter

St�ckliste:         mdAntennaAdapter_Stueckliste.ods 

Best�ckungsplan:    mdAntennaAdapter.pdf


Baugruppenfertigung:
  
  RoHS konform
  
  Pr�fung nach IPC-A-610F, Klasse 3 (optische Inspektion notwendig, R�ntgeninspektion nach Vereinbarung).
