Firma:		microdrones GmbH
		Nerzweg 5
		57072 Siegen
		Tel.: 0271/770038-0
		FAX : 0271/770038-11

Rückfragen bei	Herrn Richard Cernohorsky

Platinengröße:	ca. 65 x 65 mm² (2 Lagen)

Achtung:

Bitte Leiterplatte wie folgt aufbauen:

Oberfläche Ni/Au

Endstärke 0.5 mm

Gerberdaten in mm (Format 4.6)

mdAntennaAdapter.gtl   	Oberseite (GTL - Gerber Top Signal)
mdAntennaAdapter.gbl   	Unterseite (GBL - Gerber Bottom Signal)

mdAntennaAdapter.gts 	Stopplack Oberseite (GTS - Gerber Top Solder Mask)
mdAntennaAdapter.gbs 	Stopplack Unterseite (GBS - Gerber Bottom Solder Mask)
Die Bohrungen der Vias können freigestellt werden.

mdAntennaAdapter.gm1 	Kontur (GM1 - Gerber Mechanical 1)

Bohrdaten in mm (Excellon mit Toolliste)

mdAntennaAdapter.drl            Bohrungen

***********************************************************************************
UL-Kennzeichnung:

In den Gerberdaten ist bereits eine UL-Kennzeichnung (UL94V-0) enthalten.
Sollte Ihr Material nicht dieser Brandschutzklasse entsprechen, halten Sie bitte
Rücksprache mit uns.


***********************************************************************************
Lotpaste:

mdAntennaAdapter.gtp	Lotpasten-Schablone Oberseite (GTP - Gerber Top Paste Mask)
mdAntennaAdapter.gbp	Lotpasten-Schablone Unterseite (GBP - Gerber Bottom Paste Mask)

Achtung:

Die Schablonendaten sind unskaliert, entsprechen also der Pad-Größe.
